d3.select("#title").html("")
    .append("p")
    .text("Haz clic sobre el lote de tu interés y conoce toda la información")
    .attr("class", "textInit")

d3.select('#lote1').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote1.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote2').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote2.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote3').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote3.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote4').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote4.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote5').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote5.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote6').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote6.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote7').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote7.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote8').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote8.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote9').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote9.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote10').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote10.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote11').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote11.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote12').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote12.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote13').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote13.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote14').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote14.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote15').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote15.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote16').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote16.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote17').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote17.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote18').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote18.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote19').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote19.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote20').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote20.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote21').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote21.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote22').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote22.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote23').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote23.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote24').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote24.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote25').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote25.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote26').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote26.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});
d3.select('#lote27').on('click', function () {

    /* Parse the Data */
    d3.json("../data/lote27.json").then(function (json) {
        /*     console.log(json); */
        renderData(json);
    });

    function renderData(datos) {
        var name = d3.select("#title").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        name.insert("div")
            .html(function (d) {
                return "<h3 class='card-title'>" + d.name + "</h3>"
            })

        var area = d3.select("#area").html("")
            .selectAll("p")
            .data(datos)
            .enter()
        area.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'><span class='titleInformation'>Área:</span> " + d.area + "</p>"
            })

        var description = d3.select("#description").html("")
            .selectAll("p")
            .data(datos)
            .enter()

        description.insert("p")
            .html(function (d) {
                return "<p class='textinformation card-text'>" + d.description + "</p>"
            })

        var video = d3.select("#video").html("")
            .selectAll("div")
            .data(datos)
            .enter()

        video.insert("div")
            .attr("class", "embed-responsive embed-responsive-21by9")
            .html(function (d) {
                return "<iframe class='embed-responsive-item' src='" + d.video + "'></iframe>"
            })

        var linkDesktop = d3.select('#linkDesktop').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-none d-lg-block')
            .attr('href', '../views/contacto.html')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

        var linkPhone = d3.select('#linkPhone').html("")
            .append('a')
            .attr('class', 'btn btn_1 d-block d-lg-none')
            .attr('href', 'https://wpme.link/7d7I')
            .attr('role', 'buttom')
            .text("Quiero más información de este lote")

    }
});