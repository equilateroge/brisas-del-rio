<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="ANCARDENASC - PARDALISDIGITAL">
    <meta name="description"
        content="El Condominio Campestre “BRISAS DEL RIO”, ofrece espacios recreativos de dominio privado que te permitirán desconectarte con tu familia de la ciudad y disfrutar un ambiente tranquilo rodeado del verde fresco de la naturaleza.">
    <meta name="keywords" content="Brisas del Rio Condominio,	
Condominio Brisas del Rio,	
condominios en villeta,	
condominios en villeta cundinamarca	,
condominio en villeta para la venta	,
condominios campestres en villeta,	
condominios campestres en villeta cundinamarca,	
condominios nuevos en villeta,	
condominios villeta venta,	
condominios en villeta hoteles,	
condominios en villeta hoy,	
condominios en villeta en venta,	
condominios en villeta inmobiliaria,	
condominios en villeta guaduas,	
condominios en villeta ruta	,
condominios en villeta recorrido,	
condominios campestres villeta,	
lotes en condominio villeta cundinamarca,	
casas en condominio villeta cundinamarca,	
condominios en villeta tarifas,	
condominios en villeta telefono	,
condominios en villeta ubicacion,	
condominios en villeta web,	
casas en villeta para comprar,	
comprar casa en villeta	,
condominios campestres cerca a bogota,	
condominios cerca a bogota,	
venta de casa lotes en clima calido	,
venta de lotes en clima calido,	
venta de lotes en clima calido cundinamarca	,
venta de lotes en condominio,	
condominio en villeta para la venta,	
condominios campestres en villeta	,
condominios campestres en villeta cundinamarca	,
condominios nuevos en villeta	,
condominios villeta venta,	
condominios campestres villeta,	
lotes en condominio villeta cundinamarca,	
casas en condominio villeta cundinamarca,	
condominios en villeta cundinamarca	,
Lotes en Villeta,	
Clima de villeta">
    <meta name="robots" content="index, follow">
    <!-- stylesheet page -->
    <link rel="stylesheet" type="text/css" href="../dist/css/<?php echo $product_name ?>.min.css" media="all">
    <!--  stylesheet  framework-->
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/bootstrap-select.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/bootstrap.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/material-components-web.min.css" media="all">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/hover-min.css" media="all">
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/imagehover.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/normalize.css" media="all">
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/jquery.fancybox.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="../dist/css/framework/loadPage.css" media="all">
    <!--fonts -->
    <!--     <link href="<?php $fonts ?>" rel="stylesheet"> -->
    <!-- icos -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- favicon -->
    <link rel="shortcut icon" href="../dist/img/icos/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../dist/img/icos/favicon.ico" type="image/x-icon">
    <!-- modernizr -->
    <script src="../dist/js/framework/modernizr.js"></script>
    <script type="text/javascript" src="../dist/js/framework/d3.min.js"></script>
    <!-- title page -->
    <title><?php echo $title ?></title>

    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '172f5faa6341ae248586baa161edeb14bb1127d5';
    window.smartsupp || (function(d) {
        var s, c, o = smartsupp = function() {
            o._.push(arguments)
        };
        o._ = [];
        s = d.getElementsByTagName('script')[0];
        c = d.createElement('script');
        c.type = 'text/javascript';
        c.charset = 'utf-8';
        c.async = true;
        c.src = 'https://www.smartsuppchat.com/loader.js?';
        s.parentNode.insertBefore(c, s);
    })(document);
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-149703568-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-149703568-1');
    </script>

</head>