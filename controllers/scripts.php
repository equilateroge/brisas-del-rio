 <!--  javascript  framework-->
 <script type="text/javascript" src="../dist/js/framework/jquery.min.js"></script>
 <script type="text/javascript" src="../dist/js/framework/bootstrap.bundle.min.js"></script>
 <script type="text/javascript" src="../dist/js/framework/bootstrap.min.js"></script>
 <script type="text/javascript" src="../dist/js/framework/material-components-web.min.js"></script>
 <script type="text/javascript" src="../dist/js/framework/bootstrap-select.min.js"></script>
 <!-- javascripts gallery -->
 <script type="text/javascript" src="../dist/js/framework/jquery.fancybox.min.js"></script>
 <!--  javascript  page-->
 <script type="text/javascript" src="../dist/js/<?php echo $product_name ?>.min.js"></script>