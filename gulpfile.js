'use strict'

var config = {
  jsapp: ['./app/js/**/*.js', ],
  jsdist: ['./dist/js/', ],
  Stylessass: ['./app/sass/index.scss', ],
  Stylescssdist: ['./dist/css/', ],
  minifyimagesapp: ['./app/images/**/*.*', ],
  minifyimagesdist: ['./dist/img/', ],
  // Framework destination
  frameworkDestinationCss: ['./dist/css/framework/', ],
  frameworkDestinationjs: ['./dist/js/framework/', ],
  // Archives
  phpArchives: ['./controllers/**/*.php', './models/**/*.php', './views/**/*.php', './components/**/*.php', './index.html'],
  //production
  build: ['./build/public', ]
};

var nameProject = 'brisasDelRio';
var localhost = 'equilatero.localhost/';

var gulp = require('gulp'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  rename = require('gulp-rename'),
  sourcemaps = require('gulp-sourcemaps'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  sass = require('gulp-sass'),
  imagemin = require('gulp-imagemin'),
  mozjpeg = require('imagemin-mozjpeg'),
  pngquant = require('imagemin-pngquant'),
  changed = require('gulp-changed'),
  plumber = require('gulp-plumber'),
  htmlmin = require('gulp-htmlmin'),
  php2html = require('gulp-php2html'),
  cache = require('gulp-cache'),
  clean = require('gulp-clean'),
  browserSync = require('browser-sync').create(),
  connect = require('gulp-connect-php'),
  ftp = require('vinyl-ftp'),
  gutil = require('fancy-log'),
  reload = browserSync.reload;

/** FTP configuration **/
var user = 'ancardenasc @hotelversallesmelgar.com ';
var password = 'pardalis1020';
var host = 'ftp.hotelversallesmelgar.com';
var port = 21;
var localFilesGlob = ['./dist/**/*', './controllers/**/*', './views/**/*', './models/**/*', './index.html'];
var remoteFolder = '/pruebas.hotelversallesmelgar.com/';

// helper function to build an FTP connection based on our configuration
function getFtpConnection() {
  return ftp.create({
    host: host,
    port: port,
    user: user,
    password: password,
    parallel: 5,
    log: gutil.log,
  })
}

/////////////////////// // ////////////
// jsscripts task
//////////////////////// // ////////////
function scripts() {
  return gulp.src(config.jsapp)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(concat('temp.js'))
    .pipe(uglify())
    .pipe(rename(nameProject + '.min.js'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(config.jsdist))
    .pipe(browserSync.stream())
};
//////////////////////// // ////////////
// Styles Task
//////////////////////// // ////////////
function style () {
  return gulp.src(config.Stylessass)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(rename(nameProject + '.min.css'))
    .pipe(postcss([autoprefixer()]))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(config.Stylescssdist))
    .pipe(browserSync.stream())
};
//////////////////////// // ////////////
//Images Tasks
//////////////////////// // ////////////
 function images () {
  return gulp.src(config.minifyimagesapp)
    .pipe(plumber())
    .pipe(changed('./dist/img/'))
    .pipe(
      imagemin([
        pngquant({
          quality: [0.5, 0.5]
        }),
        mozjpeg({
          quality: 50
        }),
        imagemin.gifsicle({
          interlaced: true
        }),
        imagemin.svgo({
          plugins: [{
            removeViewBox: false,
            collapseGroups: true
          }]
        })
      ])
    )
    .pipe(gulp.dest(config.minifyimagesdist))
    .pipe(browserSync.stream())
};
//////////////////////// // ////////////
//Watch Tasks
//////////////////////// // ////////////
function watch(){
        connect.server({}, function () {
          browserSync.init({
            proxy: localhost,
            open: true  
          });
        })
      gulp.watch("./app/sass/**/*.scss", style);      
      gulp.watch("./app/js/**/*.js", scripts);
      gulp.watch("./app/images/**/*.*", images);
      gulp.watch(config.phpArchives).on('change', reload);
    }
//////////////////////// // ////////////
//Clear cache
//////////////////////// // ////////////
function cache() {
  cache.clearAll();
};
//////////////////////// // ////////////
//Exports function
//////////////////////// // ////////////
exports.watch = watch;
exports.style = style;
exports.scripts = scripts;
exports.images = images;
exports.cache = cache;
//////////////////////// // ////////////
//Default Tasks
//////////////////////// // ////////////
gulp.task('default', gulp.parallel(cache, style, scripts, images, watch));

 ////////////////////// // //////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////********** *framewroks build Tasks************////////////////////////
//////////////////////// // /////////////////////////////////////////////////////////////////////////////////////

//////// // /////
//bootstrap Tasks
//////// // /////
function frameworkCss() {
  return gulp.src(['./app/node_modules/bootstrap/dist/css/bootstrap.min.css', './app/node_modules/bootstrap-select/dist/css/bootstrap-select.min.css', './app/node_modules/normalize-css/normalize.css', './app/bookstores/gallery/jquery.fancybox.min.css', './app/bookstores/loadPage/loadPage.css', './app/node_modules/material-components-web/dist/material-components-web.min.css', './app/node_modules/hover.css/css/hover-min.css', './app/node_modules/imagehover.css/css/imagehover.min.css'], )
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(config.frameworkDestinationCss));

};
 function frameworksJs() {
  return gulp.src(['./app/node_modules/bootstrap/dist/js/bootstrap.min.js', './app/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', './app/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js', './app/node_modules/d3/dist/d3.min.js', './app/node_modules/jquery/dist/jquery.min.js', './app/js/modernizr.js', './app/bookstores/gallery/jquery.fancybox.min.js', './app/bookstores/loadPage/loadPage.js', './app/node_modules/material-components-web/dist/material-components-web.min.js'], )
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(config.frameworkDestinationjs));
};
//////// // /////
//googleColor Tasks
//////// // /////
function googleColorCss() {
  return gulp.src('./app/node_modules/google-material-color/dist/palette.scss')
    .pipe(gulp.dest('./app/sass/'));
};
//////////////////////// // ////////////
//Exports function
//////////////////////// // ////////////
exports.frameworkCss = frameworkCss;
exports.frameworksJs = frameworksJs;
exports.googleColorCss = googleColorCss;
//////// // /////
//Compile task frontend
//////// // /////
gulp.task('frontend', gulp.parallel(frameworkCss, frameworksJs, googleColorCss));


//////////////////////// // //////////////////////////////////// // //////////////////////////////////// // //////
//////////////////////////////************* */Production Tasks*********/////////////////////////////
//////////////////////// // //////////////////////////////////// // //////////////////////////////////// // //////

///////clean/////////////////////
 function clean () {
  return gulp.src(config.build, {
      read: false
    })
    .pipe(clean({
      force: true
    }));

};
///////production/////////////////////
function production() {
  return gulp.src(['./dist/**', './data/**', './font/**', './index.html'], {
      base: './'
    })
    .pipe(gulp.dest(config.build));

};
///////Deploy whit FTP/////////////////////
/* function deploy() {
  var conn = getFtpConnection()

  return gulp
    .src(localFilesGlob, {
      base: '.',
      buffer: false
    })
    .pipe(conn.newer(remoteFolder)) // only upload newer files
    .pipe(conn.dest(remoteFolder))
}; */
///////Deploy whit FIREBASE/////////////////////
function deploy(){
return gulp.src(["./views/*.php"], {base:'./'})
  .pipe(php2html())
  .pipe(htmlmin({
    collapseWhitespace: true
  }))
  .pipe(gulp.dest(config.build));
};
//////////////////////// // ////////////
//Exports function
//////////////////////// // ////////////
exports.clean = clean;
exports.production = production;
exports.deploy = deploy;
//////// // /////
//////////////////////// // ////////////
//build Tasks
//////////////////////// // ////////////
gulp.task('build', gulp.series(production, deploy));
