  <header>
      <div class="bd-example">
          <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                  <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
              </ol>
              <div class="carousel-inner">
                  <div class="carousel-item active">
                      <img src="../dist/img/brisas-11.JPG" class="d-block w-100 img-fluid" alt="...">
                      <div class="carousel-caption d-none d-lg-block rounded">
                          <h5>¡Aquí se construyen tus sueños!</h5>
                          <p>A tan solo 1 hora y 30 minutos de Bogotá, se encuentra el Condominio Campestre "BRISAS DEL
                              RIO", ubicado en el km 1.6 vía Villeta - Sasaima en la vereda Río Dulce a 5 minutos de la
                              ciudad de Villeta - Cundinamarca.
                          </p>
                          <a class="btn btn-2 hvr-grow text-uppercase mt-4" href="../views/ElCondominio.php"
                              role="button">CONOCER MÁS</a>
                      </div>
                  </div>
                  <div class="carousel-item">
                      <img src="../dist/img/brisas-2.jpg" class="d-block w-100 img-fluid" alt="...">
                      <div class="carousel-caption d-none d-lg-block rounded">
                          <h5 class="text-uppercase">Tu casa en medio de la naturaleza</h5>
                          <a class="btn btn-2 hvr-grow text-uppercase mt-4" href="../views/ElCondominio.php"
                              role="button">más información</a>
                      </div>
                  </div>
                  <div class="carousel-item">
                      <img src="../dist/img/brisas-9.jpg" class="d-block w-100 img-fluid" alt="...">
                      <div class="carousel-caption d-none d-lg-block rounded">
                          <h5>¡Conoce más sobre la ciudad dulce de Colombia!</h5>
                          <p>Entrar a nuestro blog y conoce todo lo que debes saber de Villeta</p>
                          <a class="btn btn-2 hvr-grow text-uppercase mt-4" href="#"
                              role="button">entrar al blog</a>
                      </div>
                  </div>
                  <div class="carousel-item">
                      <img src="../dist/img/brisas-10.jpg" class="d-block w-100 img-fluid" alt="...">
                      <div class="carousel-caption d-none d-lg-block rounded">
                          <h5 class="text-uppercase">¿Quieres conocer más información?</h5>
                          <p>Escríbenos y uno de nuestros asesores se contactara contigo</p>
                          <a class="btn btn-2 hvr-grow text-uppercase mt-4" href="../views/contacto.php"
                              role="button">contáctanos</a>
                      </div>
                  </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
              </a>
          </div>
      </div>
  </header>