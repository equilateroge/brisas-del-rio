<nav class="navbar navbar-expand-lg navbar-light bg-light containerNav shadow-sm">
    <a class="navbar-brand" href="../views/index.php">
        <img src="../dist/img/logo-brisas.png" alt="Logo Brisas del RIO">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span> <i class="material-icons iconMenu">
                menu
            </i></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link  menu hvr-underline-from-center px-3" href="../views/index.php">Inicio <span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link  menu hvr-underline-from-center px-3" href="../views/ElCondominio.php">El
                    condominio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  menu hvr-underline-from-center px-3" href="../views//contacto.php">Contáctanos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link menu hvr-underline-from-center px-3" href="../views/blog.php">Blog</a>
            </li>
            <li class="nav-item">
                <!--     view  desktop -->
                <a class="nav-link menu hvr-reveal px-3 d-none d-lg-block" href="https://goo.gl/maps/7d5GDDak7ZRRnMzt7">
                    <img class="img-fluid" src="../dist/img/iconLocate.png" alt=""> ¿Cómo llegar? </a>
                <!-- view mobile -->
                <a class="nav-link menu hvr-reveal px-3 d-block d-lg-none" href="https://goo.gl/maps/7d5GDDak7ZRRnMzt7">
                    ¿Cómo llegar?
                    <img class="img-fluid" src="../dist/img/iconLocate.png" alt=""></a>
            </li>
        </ul>
    </div>
</nav>