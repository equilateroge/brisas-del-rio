  <footer class="py-5">
      <div class="container">
          <div class="row">
              <div class="col-12 col-lg-8 ">
                  <div class="google-maps">
                      <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d254380.67999762716!2d-74.46887336559527!3d4.989729708542508!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e409b315531f445%3A0x3afa2dbdafb33786!2sCondominio%20Brisas%20del%20Rio!5e0!3m2!1ses-419!2sco!4v1568347739680!5m2!1ses-419!2sco"
                          width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                          class="rounded"></iframe>
                  </div>
              </div>
              <div class="col-12 col-lg-4 ">
                  <div class="containerTextFooter">
                      <p class="title-footer">El Condominio Campestre Brisas del Río está ubicado en el km 1.6 vía
                          principal Villeta - Sasaima, a
                          1 hr 30 min de la ciudad de Bogotá.</p>
                      <a class="enlace " href="tel:+573185880634">¿Tienes alguna pregunta? Llámanos al: <br> +57 3185880634</a>
                  </div>
              </div>
          </div>
      </div>
      <div class="mt-5 container-fluid copyright">
          <div class="row">
              <div class="col-12 col-lg-3 offset-lg-3">
                  <a class="nav-link mx-3 d-flex justify-content-center" href="mailto:anriapco@gmail.com">Desarrollado por anriapco@gmail.com</a>
              </div>
              <div class="col-12 col-lg-3">
                  <a class="nav-link mx-3 d-flex justify-content-center" href="#">Copyright &copy; brisas del rio <?php echo date('Y') ?></a>
              </div>
          </div>


      </div>
      <!-- /.container -->
  </footer>
  <?php require ("../controllers/scripts.php");  ?>