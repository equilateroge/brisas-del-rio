<!-- /*
 * Model PHP - Pardalis Digital (https://gitlab.com/pardalisdigital/modelphp.git)
 * Copyright 2019 Model PHP
 * Licensed under MIT (https://gitlab.com/pardalisdigital/modelphp.git)
 * -->
<?php 
    $title = 'Brisas del Rio ';
    $product_name = "brisasDelRio";
    $fonts = "https://fonts.googleapis.com/css?family=Noto+Serif|Roboto&display=swap";
    require ("../controllers/functions.php")    
?>

<body>
    <!-- Navbar -->
    <?php require ("../models/navbar.php");  ?>
    <main class="container_main my-lg-5" id="blog">
        <div class="embed-container">
            <iframe scrolling="no" src="https://brisasdelriocondominio.blogspot.com" frameborder="0"></iframe>
        </div>
    </main>
    <!-- Footer -->
    <?php require ("../models/footer.php");  ?>
</body>