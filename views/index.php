<!-- /*
 * Model PHP - Pardalis Digital (https://gitlab.com/pardalisdigital/modelphp.git)
 * Copyright 2019 Model PHP
 * Licensed under MIT (https://gitlab.com/pardalisdigital/modelphp.git)
 * -->
<?php 
    $title = 'Brisas del Rio ';
    $product_name = "brisasDelRio";
    $fonts = "https://fonts.googleapis.com/css?family=Noto+Serif|Roboto&display=swap";
    require ("../controllers/functions.php")    
?>

<body>
    <!-- Navbar -->
    <?php require ("../models/navbar.php");  ?>
    <!-- Header -->
    <?php require ("../models/header.php");  ?>
    <main class="container_main my-5 background_incio">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <img class="img-fluid rounded" src="../dist/img/brisas-5.jpg" alt="Foto casa">
                </div>
                <div class="col-12 col-lg-6 mt-2 mt-lg-0">
                    <h3 class="title-1 text-center">La casa de tus sueños</h3>
                    <p class="mt-3">El condominio cuenta con redes eléctricas, redes de acueducto con agua potabilizada, redes de
                        alumbrado público y además te permite elegir entre los diferentes modelos de casas que hemos
                        diseñado especialmente para ti.</p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 col-lg-6 order-2 order-lg-1 mt-2 mt-lg-0">
                    <h3 class="title-1 text-center">Seguridad para tu familia</h3>
                    <p class="mt-3">El Condominio BRISAS DEL RÍO te ofrece privacidad, calidad y estilo de vida, pues además de
                        contar con diferentes espacios para compartir, te da la posibilidad de: ejercitarte libremente,
                        que tus hijos puedan jugar a salvo en áreas verdes, decidir quién entra o no a tu casa y estar
                        en un entorno seguro las 24 horas del día.</p>
                </div>
                <div class="col-12 col-lg-6 order-1 order-lg-2">
                    <img class="img-fluid rounded" src="../dist/img/brisas-4.jpg" alt="Foto casa">
                </div>
            </div>
        </div>
    </main>
    <!-- Footer -->
    <?php require ("../models/footer.php");  ?>
</body>