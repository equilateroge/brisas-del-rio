<!-- /*
 * Model PHP - Pardalis Digital (https://gitlab.com/pardalisdigital/modelphp.git)
 * Copyright 2019 Model PHP
 * Licensed under MIT (https://gitlab.com/pardalisdigital/modelphp.git)
 * -->
<?php 
    $title = 'Brisas del Rio ';
    $product_name = "brisasDelRio";
    $fonts = "https://fonts.googleapis.com/css?family=Noto+Serif|Roboto&display=swap";
    require ("../controllers/functions.php")    
?>

<body id="contacto">
    <!-- Navbar -->
    <?php require ("../models/navbar.php");  ?>

    <main class="container_main my-5">
        <div class="container">

            <h2 class="titleContacto">contáctenos</h2>

            <p class="text-center mt-4">
                Para conocer más información sobre el proyecto, precios y formas de pago,
                escríbenos a
                <a href="mailto:brisasdelriocondominio@gmail.com" class="enlace">
                    brisasdelriocondominio@gmail.com​
                </a>
                <br>o simplemente completa este formulario a continuación y te contactaremos:
            <p>

            <form>
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" id="name" placeholder="Ingrese su Nombre"> 
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Ingrese su Correo"> 
                </div>
                <div class="form-group">
                    <label for="phone">Teléfono de contacto</label>
                    <input type="tel" class="form-control" id="phone" placeholder="Ingrese su Teléfono"> 
                </div>
                <div class="form-group">
                    <label for="text_area">Escriba su mensaje</label>
                    <textarea class="form-control" id="text_area" rows="3"></textarea>
                </div>            
                <button type="submit" class="btn btn_1 btn-block hvr-shrink">Enviar</button>
            </form>



        </div>
    </main>
    <!-- Footer -->
    <?php require ("../models/footer.php");  ?>
</body>