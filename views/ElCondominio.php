<!-- /*
 * Model PHP - Pardalis Digital (https://gitlab.com/pardalisdigital/modelphp.git)
 * Copyright 2019 Model PHP
 * Licensed under MIT (https://gitlab.com/pardalisdigital/modelphp.git)
 * -->
<?php 
    $title = 'Brisas del Rio ';
    $product_name = "brisasDelRio";
    $fonts = "https://fonts.googleapis.com/css?family=Noto+Serif|Roboto&display=swap";
    require ("../controllers/functions.php")    
?>

<body id="condominio">
    <!-- Navbar -->
    <?php require ("../models/navbar.php");  ?>

    <main class="container_main my-5">
        <div class="container">
            <div class="row mb-5">
                <div class="col-12 col-lg-7">
                    <img class="rounded img-fluid" src="../dist/img/brisas-6.jpg" alt="Fotografia brisas condominio">
                </div>
                <div class="col-12 col-lg-5 mt-3 mt-lg-0">
                    <p>
                        El Condominio Campestre<span class="emphatic"> “BRISAS DEL RIO”</span>, ofrece espacios
                        recreativos de dominio privado
                        que te permitirán desconectarte con tu familia de la ciudad y disfrutar un ambiente
                        tranquilo rodeado del verde fresco de la naturaleza.
                    </p>

                    <div class="row">
                        <div class="col-6">
                            <div>
                                <picture id="icon1" class="mr-2 animated bounceIn"></picture>
                                <p class="py-3">Cancha múltiple</p>
                            </div>

                        </div>
                        <div class="col-6">
                            <div>
                                <picture id="icon2" class="mr-2 animated bounceIn"></picture>
                                <p class="py-3">Sede social</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div>
                                <picture id="icon3" class="mr-2 animated bounceIn"></picture>
                                <p class="py-3">Senderos ecológicos</p>
                            </div>

                        </div>
                        <div class="col-6">
                            <div>
                                <picture id="icon4" class="mr-2 animated bounceIn"></picture>
                                <p class="py-3">Parque infantil</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div>
                                <picture id="icon5" class="mr-2 animated bounceIn"></picture>
                                <p class="py-3">Zona BBQ</p>
                            </div>

                        </div>
                        <div class="col-6">
                            <div>
                                <picture id="icon6" class="mr-2 animated bounceIn"></picture>
                                <p class="py-3">Zona de parqueadero</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row my-5">
                <div class="col-12 col-lg-5 mt-3 mt-lg-0 order-2 order-lg-1">
                    <p>
                        El Proyecto de vivienda campestre cuenta con <span class="emphatic"> 27 lotes que comprenden
                            áreas desde 1.000 m² hasta 4.500 m²</span> con vías adoquinadas y se encuentran rodeados de
                        bosques verdes, árboles frutales, jardines florales, variedad de hermosas aves, y además están
                        cobijados por la fresca brisa del río que bordea el Condominio.
                    </p>
                    <a class="btn btn_1 hvr-pulse btn-block" href="../views/lotes.php" role="button">Ver mapa de
                        lotes</a>
                </div>
                <div class="col-12 col-lg-7  order-1 order-lg-2">
                    <img class="rounded img-fluid" src="../dist/img/brisas-8.jpg" alt="Fotografia brisas condominio">
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-12 col-lg-7">
                    <img class="rounded img-fluid" src="../dist/img/brisas-12.jpg" alt="Fotografia brisas condominio">
                </div>
                <div class="col-12 col-lg-5 mt-3 mt-lg-0">
                    <p>
                        Tu futuro se construye cerca a Bogotá y hoy puedes hacer parte del <span class="emphatic">
                            Condominio Brisas del
                            Río</span> que te ofrece una vida única, tranquila y diferente.
                        <br>
                        Haz parte de este gran proyecto con Navashe Ravelotti Constructora.
                    </p>
                    <a class="btn btn_1 hvr-pulse btn-block" href="../views/contacto.php" role="button">Reservar
                        una cita</a>
                </div>
            </div>

        </div>
    </main>
    <!-- Footer -->
    <?php require ("../models/footer.php");  ?>
</body>