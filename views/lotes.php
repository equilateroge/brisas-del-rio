<!-- /*
 * Model PHP - Pardalis Digital (https://gitlab.com/pardalisdigital/modelphp.git)
 * Copyright 2019 Model PHP
 * Licensed under MIT (https://gitlab.com/pardalisdigital/modelphp.git)
 * -->
<?php 
    $title = 'Brisas del Rio ';
    $product_name = "brisasDelRio";
    $fonts = "https://fonts.googleapis.com/css?family=Noto+Serif|Roboto&display=swap";
    require ("../controllers/functions.php")    
?>

<body id="lotes">
    <!-- Navbar -->
    <?php require ("../models/navbar.php");  ?>

    <div id="backgroundLotes" class="embed-responsive" >
        <img src="../dist/img/photoBackgrund.jpg" alt="">
    </div>

    <main class="container_main">
        <div class="container-fluid">
            <div class="  titleMap">                
                <img src="../dist/img/mapaTitle.svg" alt="" class="img-fluid ">
            </div>
            <div class="row">
                <div class="col-12 col-lg-7">
                    <?php require ("../components/polygons/poligonoV1.php")?>
                </div>
                <div class="col-12 col-lg-5">
                    <div class="mt-5 mb-5">
                        <div class="card">
                            <div class="card-body">
                                <div id="title"></div>
                                <p id="area"></p>
                                <p id="description"></p>
                                <div id="video"></div>
                                <div class="d-flex justify-content-center" id="linkDesktop"></div>
                                <div class="d-flex justify-content-center" id="linkPhone"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </main>

    <!-- Footer -->
    <?php require ("../models/footer.php");  ?>
</body>